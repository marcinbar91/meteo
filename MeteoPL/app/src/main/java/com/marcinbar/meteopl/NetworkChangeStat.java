package com.marcinbar.meteopl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChangeStat extends BroadcastReceiver {

    public static boolean Type_Current_Connected = false;
    static Intent serviceIntent;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        getConnectivityStatus(context);
    }


    public static void getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting() && Type_Current_Connected == false) {
            serviceIntent = new Intent(context, AlarmService.class);
            context.startService(serviceIntent);
            Type_Current_Connected = true;
        }
        else if(activeNetwork == null && Type_Current_Connected) {
            context.stopService(serviceIntent);
            Type_Current_Connected = false;
        }
    }

}