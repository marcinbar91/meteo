package com.marcinbar.meteopl;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

public class Notifi {

    static NotificationManager notificationManager;

    public static void Show(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String date = preferences.getString("date", null);

        notificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        final NotificationCompat.Builder notif = new NotificationCompat.Builder(context.getApplicationContext())
                .setContentTitle("MeteoPl")
                .setContentText("Ostatnia aktualizacja: "+ date)
                .setSmallIcon(R.drawable.ic_logo_icm)
                .setPriority(Notification.PRIORITY_MAX)
                .setOngoing(true);
        notificationManager.notify(1, notif.build());
    }

    public static void CancelAll() {
        if (notificationManager != null) {
            notificationManager.cancelAll();
            notificationManager = null;
        }
    }




}
