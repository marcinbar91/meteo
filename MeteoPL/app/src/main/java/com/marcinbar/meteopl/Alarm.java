package com.marcinbar.meteopl;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Alarm extends BroadcastReceiver
{
    int x;
    int y;

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();


        Calendar rightNow = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        GetLocalization(context);
        String link = "http://www.meteo.pl/um/metco/mgram_pict.php?ntype=0u&fdate=" + sdf.format(rightNow.getTime()) + "00&row="+x+"&col="+y+"&lang=pl";
        new DownloadFile(context).execute(link);

        wl.release();
    }

    private void GetLocalization(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String value = pref.getString("localization_preference", "Lublin");


        TypedArray icons;

        switch (value) {
            case "Lublin":
                icons = context.getResources().obtainTypedArray(R.array.Lublin);
                x = icons.getInt(0, 0);
                y = icons.getInt(1, 0);
                Log.d(String.valueOf(x), String.valueOf(y));
                break;
            case "Kielce":
                icons = context.getResources().obtainTypedArray(R.array.Kielce);
                x = icons.getInt(0, 0);
                y = icons.getInt(1, 0);
                Log.d(String.valueOf(x),String.valueOf(y));
                break;
            case "Warszawa":
                icons = context.getResources().obtainTypedArray(R.array.Warszawa);
                x = icons.getInt(0, 0);
                y = icons.getInt(1, 0);
                Log.d(String.valueOf(x),String.valueOf(y));
                break;
        }
    }

    AlarmManager alarmManager;
    PendingIntent sender;
    public void SetAlarm(Context context)
    {

        Intent intent = new Intent(context, Alarm.class);
        sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 *60 *4, sender);
    }

    public void CancelAlarm()
    {
        if (alarmManager!= null) {
            alarmManager.cancel(sender);
        }

    }
}