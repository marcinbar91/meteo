package com.marcinbar.meteopl;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Calendar;

public class DownloadFile extends AsyncTask<String,Integer,Long> {

    String dir = Environment.getExternalStorageDirectory()+ "/MeteoPL/";
    Context context;
    DownloadFile(Context c)
    {
        this.context = c;
    }

    @Override
    protected void onPreExecute()
    {
        CreateDir();
    }
    @Override
    protected Long doInBackground(String... aurl) {
        try {
            URL url = new URL((String) aurl[0]);
            InputStream input = url.openStream();
            File storagePath =new File(dir);
            OutputStream output = new FileOutputStream (new File(storagePath,"meteo.png"));
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                output.write(buffer, 0, bytesRead);
            }
            output.close();
            input.close();
        } catch (Exception e) {}
        return null;
    }

    @Override
    protected void onPostExecute(Long result)
    {
        super.onPostExecute(result);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("date", String.valueOf(Calendar.getInstance().getTime()));
        editor.commit();
        Notifi.Show(context);
        write2file(String.valueOf(Calendar.getInstance().getTime()));
    }

    private void write2file(String log) {
        FileWriter f;
        try {
            f = new FileWriter(Environment.getExternalStorageDirectory()+ "/MeteoPL/log.txt",true);
            f.write(log);
            f.write("\n");
            f.flush();
            f.close();
        }catch (Exception e){
        }
    }

    private void CreateDir() {
        File folder = new File(dir);
        if(!folder.exists()){
            folder.mkdir();
        }
    }
}