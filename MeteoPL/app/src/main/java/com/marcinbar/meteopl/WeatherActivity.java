package com.marcinbar.meteopl;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;
import android.webkit.WebView;


public class WeatherActivity extends FragmentActivity {

    public static WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        wv = (WebView) findViewById(R.id.webView2);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setAllowFileAccess(true);

        SetImage();
    }

    public static void SetImage() {
        String imagePath = "file://"+ Environment.getExternalStorageDirectory() + "/MeteoPL/meteo.png";
        String html = "<html><head></head><body><img src=\""+ imagePath + "\"></body></html>";
        wv.loadDataWithBaseURL("", html, "text/html","utf-8", "");
    }


    @Override
    public void finish() {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();
        super.finish();
    }
}
