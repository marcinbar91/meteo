package com.marcinbar.meteopl;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

public class MainActivity extends ActionBarActivity {

    private Button btnLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLoad = (Button) findViewById(R.id.BTNload);

        FirstRun();
        NetworkChangeStat.getConnectivityStatus(this);

        BtnOnClickListeners();

        Notifi.Show(this);
    }



    private void FirstRun() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getString("date", null) == null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("date", String.valueOf(Calendar.getInstance().getTime()));
            editor.commit();
        }
    }



    private void BtnOnClickListeners() {
        btnLoad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startAnotherActivity = new Intent(getApplicationContext(), WeatherActivity.class);
                startActivity(startAnotherActivity);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent setting = new Intent(this, PreferencesActivity.class);
            startActivity(setting);
            return true;
        }
        if (id == R.id.action_exit) {
            Intent serviceIntent = new Intent(this, AlarmService.class);
            stopService(serviceIntent);
            Notifi.CancelAll();
            System.exit(0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}